#!/usr/bin/python3
from dmm.modules.fallocate import fallocate
import os

def test_fallocate_create_file():
    fallocate.create_file("4M", "/tmp/test.img")
    file_stats = os.stat("/tmp/test.img")
    # 4M is 4194304
    assert file_stats.st_size == 4194304
    os.remove("/tmp/test.img")
