"""
losetup functions for for dmm.
"""

import os

def init():
    """
    Initialization for losetup module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for losetup module
    """
    if config['function'] == 'unmount':
        os.system("losetup -d /dev/%s" % config['loopdev'])
    else:
        os.system("losetup -P /dev/%s %s" % (config['loopdev'], config['diskimg']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    # TODO
    return ({'parted': {"priority": "required", \
             "description": "disk partition manipulator"} })

init()
