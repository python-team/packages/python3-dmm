"""
mkfs functions for for dmm.
"""

import os

def init():
    """
    Initialization for mkfs module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for mkfs module
    """
    for filesystem in config['partitions']:
        os.system("mkfs.%s %s %s" % (filesystem['fstype'], filesystem['options'], filesystem['partition']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    # TODO
    return ({'parted': {"priority": "required", \
             "description": "disk partition manipulator"} })

init()
