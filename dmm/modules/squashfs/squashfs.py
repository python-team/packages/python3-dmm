"""
mksquashfs functions for for dmm.
"""

import os

def init():
    """
    Initialization for mksquashfs module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for mksquashfs module
    """
    if config['action'] == 'make-squashfs':
        blocksize = config['blocksize']
        compress = config['compression-method']
        source = config['source']
        dest = config['destination']
        opts = config['options']
        os.system("mksquashfs %s %s -b %s -comp %s %s" % (source, dest, blocksize, compress, opts))

    #if config['action'] == unsquashfs:
        # todo


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'squashfs-tools-ng': {"priority": "required", \
             "description": "set of tools for working with squashfs images"} })

init()
