"""
debootstrap functions for for dmm.
"""

import os

def init():
    """
    Initialization for debootstrap module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for debootstrap module
    """
    os.system("debootstrap %s %s %s %s" % (config['debootstrapopts'],
                                                    config['release'],
                                                    config['destination'],
                                                    config['mirror']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    # TODO
    return ({'parted': {"priority": "required", \
             "description": "disk partition manipulator"} })

init()
