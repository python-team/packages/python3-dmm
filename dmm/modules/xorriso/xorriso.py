"""
xorisso functions for dmm (not yet implemented)
"""

import os

def init():
    """
    Initialization for xorisso module
    """


def recipe_run(config, globalconf):
    """
    Misc setup tasks for virtual machines
    """
# debian command:
# xorriso -outdev /external/out/free/amd64/iso-hybrid.tmp/debian-live-testing-amd64-cinnamon.iso -volid d-live testing ci amd64 -padding 0 -compliance no_emul_toc -map /w/work/amd64-free-cinnamon/tmp/tmpc6RGMU / -chmod 0755 / -- -boot_image isolinux dir=/isolinux -boot_image isolinux system_area=/usr/lib/ISOLINUX/isohdpfx.bin -boot_image any next -boot_image any efi_path=boot/grub/efi.img -boot_image isolinux partition_entry=gpt_basdat


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'xorriso': {"priority": "required", \
             "description": "ISO-9660 and Rock Ridge manipulation tool"} })

init()
