"""
delete functions for for dmm.
"""

import os

def init():
    """
    Initialization for delete module
    """


def recipe_run(config, globalconf):
    """
    Perform recipe actions for delete module
    """
    for path in config['paths']:
        delete_path(path['path'])


def delete_path(path):
    print("Removing %s" % path)
    os.system("rm -rf %s" % path)


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'coreutils': {"priority": "required", \
             "description": "GNU core utilities"} })

init()
