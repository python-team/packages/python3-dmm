"""
mmdebstrap functions for for dmm.
"""

import os

def init():
    """
    Initialization for mmdebstrap module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for mmdebstrap module
    """
    os.system("mmdebstrap %s %s %s %s" % (config['debootstrapopts'],
                                                    config['release'],
                                                    config['destination'],
                                                    config['mirror']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'mmdebstrap': {"priority": "required", \
             "description": "create a Debian chroot"} })

init()
