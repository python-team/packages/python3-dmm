"""
parted functions for for dmm.
"""

import os

def init():
    """
    Initialization for parted module
    """


def recipe_run(config, globalconf):
    """
    Creates partitions on device.
    """
    for partedcmd in config['static-layout']:
        os.system("parted -s -- %s %s" % (config['destination'], partedcmd))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'parted': {"priority": "required", \
             "description": "disk partition manipulator"} })

init()
