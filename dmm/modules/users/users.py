"""
aptsetup functions for for dmm.
"""

import os

def init():
    """
    Initialization for aptsetup module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for aptsetup module
    """
    for user in config['users']:
        print("username: " + user['username'])
        password_hash = os.popen("mkpasswd --method=SHA-512 " + user['password']).read().replace('\n', '')
        os.system("chroot /tmp/disk useradd -p '%s' %s" % (password_hash, user['username']))
        os.system("chroot /tmp/disk mkdir home/%s" % user['username'])
        os.system("chroot /tmp/disk chown %s home/%s" % (user['username'], user['username']))
        if user['sudo']:
            os.system("chroot /tmp/disk adduser %s sudo" % user['username'])
            os.system("chroot /tmp/disk apt-get install sudo")


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'apt': {"priority": "required", \
             "description": "commandline package manager"} })

init()
