"""
networking functions for for dmm.
"""

import os

def init():
    """
    Initialization for networking module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for aptsetup module
    """
    text_file = open(config['chroot'] + "/etc/hostname", "w")
    hostname_write = text_file.write(config['hostname'])
    text_file.close()


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'apt': {"priority": "required", \
             "description": "commandline package manager"} })

init()
