"""
umountfs functions for for dmm.
"""

import os

def init():
    """
    Initialization for umountfs module
    """

def recipe_run(config, globalconf):
    """
    Perform actions for umountfs module
    """
    for mount in config['mounts']:
        os.system("umount %s" % (mount['mountpoint']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'mount': {"priority": "required", \
             "description": "tools for mounting and manipulating filesystems"} })


init()
