"""
write some data to files for dmm
"""

import pathlib

# todo: add options to allow appending/replacing

def init():
    """
    Initialization for writefile module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for writefile module
    """
    if config['action'] == 'write_files':
        for file in config['files']:
            write_file(file['destination'], file['content'])


def write_file(file, content):
    """
    Writes one or more lines to a file.
    """
    # Ensure that basepath exists first
    path = pathlib.Path(file).parent
    path.mkdir(parents=True, exist_ok=True)

    file = open(file, "w+")
    file.write(content)
    file.close()


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    return ({'curl': {"priority": "required", \
             "description": "command line tool for transferring data with URL syntax"} })

init()
