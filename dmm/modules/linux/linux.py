"""
mkfs functions for for dmm.
"""

import os

def init():
    """
    Initialization for linux module
    """


def recipe_run(config, globalconf):
    """
    Perform actions for linux module
    """
    os.system("chroot %s apt-get -y -qq install %s" % (config['chroot'], config['package']))


def depends():
    """
    Returns a list of dependencies for this module.
    Currently this is Debian packages.
    """
    # TODO
    return ({'parted': {"priority": "required", \
             "description": "disk partition manipulator"} })

init()
