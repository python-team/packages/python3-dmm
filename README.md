Welcome to distribution management modules!
-------------------------------------------

This is all still in a very alpha state, but, you could probably already
use it to build some basic system images.

The goal is to include various useful modules that are typically used for
creating images (containers, boot images, VMs, live images), installing bare
metal machines and building and publishing packages. The initial release aims
to have modules for grub, squashfs, {de/mm/etc}bootstrap, apt, dd, xorisso
and similar tools.

The dmm.py python module along with the dmm-perform-recipe tool allows you
to string together actions for these modules in one larger recipe.

The modules indluded in dmm are meant for generic common tasks when it comes
to putting together a distribution. They are not meant to be general python
modules for the tools mentioned above. For example, if you are building a tool
that specifically needs to interact with APT, then you should use something
like the python3-apt package instead.

modules
-------

Modules are stored in subdirectories under src/modules. They are simply
python modules. The collection of modules shipped with distro-installer
are considered as the standard dmm modules collection. 3rd party modules can
be added to distro-installer or even replace standard ones by simple
configuration.

DMM is completely modular. This means you can choose to use partman or fdisk
or your own module for the partitioner. Or you can choose between something
like using debootstrap to install a system, or extract from tarball. You can
also use a combination of modules to get the best of either.

Each module takes an action, and along with that some parameters for that
action. For example, if you call the apt module, you can specify an action
to update, remove or install packages, with the parameters being the packages
that you'd like to add or remove. Some parameters might be optional and others
mandatory.

More module documentation will follow! :)

anatomy of a module
-------------------

A module is just a bunch of functions, it should call it's own init
function when imported that does the minimum initiation needed.

It must have a depends() function that returns the list of dependencies
it needs, and specify both a description of why it's needed and whether
it's required or optional. Later on, dmm will be able to tell you which
Debian packages it needs to perform the actions it needs based on the
enabled modules.

dependencies
------------

core (essential):
python3-yaml python3-loguru

dependend on task (recommended):
squashfs-tools xorriso reprepro grub-common mtools grub-pc-bin
uuid-runtime debootstrap debsums

licence
-------

Copyright 2022 Jonathan Carter

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
USE OR PERFORMANCE OF THIS SOFTWARE.

